package br.com.itau.investimento.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.investimento.models.Cliente;
import br.com.itau.investimento.services.ClienteService;

@RestController
@RequestMapping("/cliente")
public class ClienteController {
	@Autowired
	ClienteService clienteService;

	@PostMapping
	public ResponseEntity criar(@RequestBody Cliente cliente){
		cliente = clienteService.cadastrar(cliente);
		
		return ResponseEntity.status(201).body(cliente);
	}
}
